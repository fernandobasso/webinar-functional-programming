# Webinar Functional Programming


With a lib like Ramda or just plain awesome vanilla ECMAScript?

Only talk about concepts and show small, isolated code examples or refactor some
procedural or object oriented code to a functional style?
