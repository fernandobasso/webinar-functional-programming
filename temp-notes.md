# Temporary Notes


```js
const exist = a => a !== undefined || a !== null;

const normalize = when(exist,
  over(lensprop('phonewhatsapprequired'), pipe(defaultto(''), formatters.phone.format))
);

/**
 * get the phone's area code.
 *
 * @param {string} phone
 * @return {string}
 */
const getphonearea = phone => phone.substring(0, 2);

const getphoneareaandnumber = ({ whatsappphone, cellphone, homephone }) => {
  if (whatsappphone && iscellphone(whatsappphone)) return { area: getphonearea(whatsappphone), number: getphonenumber(whatsappphone) };
  if (cellphone && iscellphone(cellphone)) return { area: getphonearea(cellphone), number: getphonenumber(cellphone) };
  if (homephone && iscellphone(homephone)) return { area: getphonearea(homephone), number: getphonenumber(homephone) };
  return { area: '', number: '' };
};

const getphoneareaandnumberr = ({ whatsappphone, cellphone, homephone }) => pipe(
  find(iscellphone),
  formatters.phone.format,
)([whatsappphone, cellphone, homephone]);
```
